import React, {Fragment, useContext, useState } from 'react';
import {
  Button,
  FlatList,
  TouchableOpacity,
  Image,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions
} from 'react-native';
import { ImageBackground, Tile, Overlay, Title, Caption,
         Screen, NavigationBar, ListView, GridRow, Icon} from '@shoutem/ui';
import {graphql, QueryRenderer} from 'react-relay';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import IOSIcon from "react-native-vector-icons/Ionicons";

import { DataContext } from '../DataContext';
import environment from '../Environment';
import { ITEMS_PER_PAGE } from './Constants'
import Channels from './Channels'

const HomeQuery = graphql`
  query HomeScreen_Query($first: Int!, $after: String) {
    me {
        ...Channels_list @arguments (first: $first,
                                     after: $after)   
    } 
  }
`;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    backgroundColor: '#000'
  },
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },  
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
    color: '#fff'
  },
});

const HomeScreen = (props) => {

    const context = useContext(DataContext);
    [dimensions, setDimensions] = useState({
          width: Dimensions.get('window').width,
          height: Dimensions.get('window').height
    });

    Dimensions.addEventListener("change", (e) => {
        setDimensions({
            width: e.window.width,
            height: e.window.height
        })
    });



    return <QueryRenderer
              environment={environment}
              query={HomeQuery}
              variables={{
                  "first": ITEMS_PER_PAGE
              }}              
              render={ ({error, props}) => {
                  if (error) {
                      return <Text>{error.message}</Text>;
                  }
                  if (!props) {
                      return (<Text>Loading...</Text>);
                  }

                  return (
                    <Screen style={styles.container}>
                      <View style={{ dimensions }}>
                          <Channels list={props.me} />
                        </View>                    
                    </Screen>)
              }}
            />
}

// HomeScreen.navigationOptions = {
//   title: 'Home',
//   drawerLabel: 'Home',
//   drawerIcon: ({ tintColor }) => (
//       <Image
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//   ),  
//   headerLeft: (<TouchableOpacity onPress={() => props.navigation.navigate("DrawerOpen")}>
//                   <IOSIcon name="ios-menu" size={30} />
//                </TouchableOpacity>)
// };

export default HomeScreen;