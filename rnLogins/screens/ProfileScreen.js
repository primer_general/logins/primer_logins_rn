import React, { useContext } from 'react';
import { View, Text, Image, Button } from 'react-native';
import { LoginManager } from 'react-native-fbsdk'

import { DataContext } from '../DataContext';

const ProfileScreen = (props) => {

    const context = useContext(DataContext);
    const { user } = context;

    return(
        <View>
          <Text>Profile</Text>
          <Image source={{ uri: user.picture_url}} 
                style={{width: 150, height: 150, borderRadius: 150/2}}/>
          <Text>{user.name}</Text>
          <Text>{user.email}</Text>
          <Button title='Logout' onPress={ () => {
            LoginManager.logOut();
            props.navigation.navigate('Login');
          }} />
        </View>

    );
}

ProfileScreen.navigationOptions = {
  title: 'Profile',
  drawerLabel: 'Profile'
}

export default ProfileScreen;