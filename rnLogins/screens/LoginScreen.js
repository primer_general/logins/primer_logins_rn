// @flow
import React, {Fragment, useContext} from 'react';
import {
    SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableHighlight,
  Button,
  Dimensions
} from 'react-native';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';
import AsyncStorage from '@react-native-community/async-storage';

import { LoginButton, AccessToken, LoginManager, 
        GraphRequest, GraphRequestManager } 
from 'react-native-fbsdk';

import Video from 'react-native-video'; 

import { DataContext } from '../DataContext';
import { User } from '../models/User';

const { height } = Dimensions.get("window");

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundVideo: {
    height: height,
    position: "absolute",
    top: 0,
    left: 0,
    alignItems: "stretch",
    bottom: 0,
    right: 0
  }
});

const LoginScreen = (props) => {

    const context = useContext(DataContext);
    let access_token: ?string = '';

    const _responseInfoCallback = async(error, result) => {
        if ( result ) {
            const user = new User(result.name, result.email, 
                        `https://graph.facebook.com/${result.id}/picture?type=normal`, 
                         context.token,
                         "facebook"); 
            
            try {
                await AsyncStorage.setItem('@token', user.token)
            } catch( error ) {
                console.error(error)
            }
            context.updateUser(user);

            props.navigation.navigate('Home');
        } else {
            console.error('Error fetching data: ' + JSON.stringify(error));
        }
    }

    const initUser = (token) => {

        const infoRequest = new GraphRequest(
            '/me',
            {
                accessToken: token,
                parameters: {
                    fields: {
                        string: "id,name,email"
                    }
                }
            },
            _responseInfoCallback
        );

        new GraphRequestManager().addRequest(infoRequest).start();
    }

    AccessToken.getCurrentAccessToken()
    .then( data => {
        if( data && data.accessToken) {
            const { accessToken } : {accessToken: string} = data;
            access_token = accessToken;
            context.updateToken(accessToken);
            initUser(accessToken);
        } else {
            throw new Error('no token was stored previously')
        }
    })
    .catch( error => console.log(error))

    const user = {};
    return (
        <View style={styles.MainContainer}>
            {/* <Video
                source={require("./../assets/video1.mp4")}
            /> */}
            <Text>Please log in</Text>
            <LoginButton
                publishPermissions={["email"]}
                onLoginFinished={
                     (error, result) => {

                        if (error) {
                            alert("Login failed with error: " + error.message);
                        } else if (result.isCancelled) {
                            alert("Login was cancelled");
                        } else {
                            AccessToken.getCurrentAccessToken()
                            .then( (data) => {
                                if( data ) {
                                    const { accessToken } : {accessToken: string } = data;
                                    context.updateToken(accessToken);
                                    access_token = accessToken;
                                    initUser(accessToken)
                                }
                            })
                        }
                    }
                }
                onLogoutFinished={() => alert("User logged out")}/> 
        </View>        
    )

}

export default LoginScreen;