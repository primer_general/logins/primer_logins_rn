import React from 'react'
import { graphql, createPaginationContainer } from 'react-relay';

import { ListView } from '@shoutem/ui';
import Channel from './Channel'         

const Channels = (props) => {

    const renderRow = ({node}) => {
      return <Channel item={node}/>
    }

    return (
        <ListView
            data={props.list.channels.edges}
            renderRow={renderRow}
        />  
        
    )
}

export default createPaginationContainer(
Channels,
{
    list: graphql`
        fragment Channels_list on User
            @argumentDefinitions(
                first: { type: "Int!" }
                after: { type: "String" }
            ) 
            {
                channels(first: $first, after: $after) 
                        @connection(key: "Channels_channels"){
                    totalCount
                    pageInfo {
                        hasNextPage
                        endCursor
                    }
                    edges {
                        cursor
                        node {
                            ...Channel_item
                        }
                    }
                }        
            }    
    `},
{
    direction: 'forward',
    query: graphql`
        query Channels_Query($first: Int!,
                            $after: String) {
            me {
                ...Channels_list @arguments(first: $first,
                                        after: $after)
            }
        }
    `,
    getConnectionFromProps(props) {
        return props.list && props.list.channels
    },
    getFragmentVariables: (prevVars, totalCount) => {
      return {
        ...prevVars,
        count: totalCount
      }
    },
    getVariables: (props, {count, cursor}, fragmentVariables) => {
      return {
        first: count,
        after: cursor
      }
    }        
} )