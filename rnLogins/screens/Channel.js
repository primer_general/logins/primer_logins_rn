import React from 'react'
import {graphql, createFragmentContainer} from 'react-relay';
import {
  TouchableOpacity,
} from 'react-native';
import { ImageBackground, Tile, Overlay, Title, Caption } from '@shoutem/ui';

const Channel = ({item}) => {

    const channelPressed = (channel) => {
      console.log(channel)
    }

    return (
          <TouchableOpacity onPress={ () => channelPressed(item)}>
            <ImageBackground 
                styleName="featured"
                source={{ uri: item.poster }}>
                <Tile>
                  <Overlay>
                    <Title styleName="md-gutter-bottom">{item.name}</Title>
                    <Caption>{item.description}</Caption>
                  </Overlay>
                </Tile>
            </ImageBackground>
          </TouchableOpacity>

    )
}

export default createFragmentContainer(
Channel,
{
    item:  graphql`
        fragment Channel_item on Channel {
            name
            description
            poster
            address
        }`
});
