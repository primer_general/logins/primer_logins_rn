// @flow
import {commitMutation, graphql} from 'react-relay';

import type { RegisterMutation, 
              RegisterMutationVariables } from './__generated__/RegisterMutation.graphql.js';
import environment from '../Environment';
import type { User } from '../models/User';

const mutation = graphql`
    mutation RegisterMutation($user: UserInput!) {
        register(user: $user) 
    }  
`;

const commit = (user: User) => {

    const userInput: UserInput = {
        name: user.name,
        email: user.email,
        pictureUrl: user.picture_url,
        token: user.token,
        providerName: user.providerName
    };

    const variables: RegisterMutationVariables = {
        user: userInput
    };

    commitMutation(environment,
                    {
                        mutation,
                        variables,
                        onCompleted: (response, errors) => {
                            errors && console.error(errors)
                        },
                        onError: (error) => {
                            console.error(error);
                        },
                    }
    )

}

export default {commit};

