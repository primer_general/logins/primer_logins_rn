import React from 'react';

export const DataContext = React.createContext({});

export const DataProvider = DataContext.Provider;
export const DataConsumer = DataContext.Consumer;

import RegisterMutation from './mutations/RegisterMutation';

export const withAppContextProvider = ChildComponent => props => {

    const user = {
        user: User = {},
        updateToken: function (token) {
            this.token = token;
        },
        updateUser: function(user: User) {
            this.user = user;
            this.token = user.token;
            RegisterMutation.commit(user);
        }
    };

    return (
        <DataProvider value={user}>
            <ChildComponent {...props} />
        </DataProvider>
    )
};

export const withAppContext = ChildComponent => props => (
  <DataConsumer>
    {context => <ChildComponent {...props} global={context} />}
  </DataConsumer>
);