// @flow
import React from 'react';
import { Platform } from 'react-native';
import {
    Environment,
    Network,
    RecordSource,
    Store,
    QueryResponseCache
  } from 'relay-runtime';

import { RelayNetworkLayer, 
        urlMiddleware, 
        authMiddleware,
        cacheMiddleware 
} from 'react-relay-network-modern';
import AsyncStorage from '@react-native-community/async-storage';

const SERVER = Platform.OS === 'ios' ? 'http://localhost:4000' : 'http://10.0.2.2:4000';

const network = new RelayNetworkLayer([
    urlMiddleware({
        url: (req) => Promise.resolve(SERVER),
    }),
    authMiddleware({
        token: async() => {
            try {
                const value = await AsyncStorage.getItem('@token');
                return value;
            } catch( error ) {
                console.error(error);
            }
        }
    }),
    cacheMiddleware({size: 100, ttl: 100000 })
]
);

const environment = new Environment({
    network: network,
    store: new Store(new RecordSource()),
});

  export default environment;