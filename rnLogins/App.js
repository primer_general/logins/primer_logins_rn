/**
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator  } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import ProfileScreen from './screens/ProfileScreen';

const NavigationDrawerStructure = ({navigationProps}) => {
  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={ () => navigationProps.toggleDrawer() }>
        <Image 
          source={require('./images/drawer.png')}
          style={{ width: 25, height: 25, marginLeft: 5 }}/>
      </TouchableOpacity>    
    </View>      
  )
}

const Home_StackNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({navigation}) => ({
      title: 'Home',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#FFF'
    })
  }
});

const Profile_StackNavigator = createStackNavigator({
  Profile: {
    screen: ProfileScreen,
    navigationOptions: ({navigation}) => ({
      title: 'Home',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#FF9800'
      },
      headerTintColor: '#FFF'
    })
  }    
});

const AppNavigator = createDrawerNavigator({
    Home: {
      screen: Home_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Home'
      }
    },
    Profile: {
      screen: Profile_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Profile'
      }
    }
});

const switchNavigator  = createSwitchNavigator({
    Login: LoginScreen,
    App: AppNavigator
  }, 
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  } 
)

export default createAppContainer(switchNavigator);

