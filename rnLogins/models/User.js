// @flow

export class User {
    id: string;

    name: string;
    email: string;
    picture_url: string;
    token: ?string;
    providerName: string;

    constructor(name: string, email: string, picture_url: string, token: string, providerName: string) {
        this.name = name;
        this.email = email;
        this.picture_url = picture_url;
        this.token = token;
        this.providerName = providerName;
    }
}